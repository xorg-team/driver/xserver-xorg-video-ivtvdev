/*
X11 video driver for the Conexant 23415 MPEG encoder/decoder On-Screen Display
using the ivtvfb framebuffer driver.

Copyright (C) 2004 Matthias Badaire
Copyright (C) 2004-2007 John P. Harvey <john.p.harvey@btinternet.com>
Copyright (C) 2006-2007 Ian Armstrong <ian@iarmst.demon.co.uk>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FIT-
NESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
XFREE86 PROJECT BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Except as contained in this notice, the name of the XFree86 Project shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization from the XFree86 Project.
*/

#ifndef _IVTV_HW_H_
#define _IVTV_HW_H_

#include "xf86str.h"
#include "colormapst.h"
#include <asm/ioctl.h>
#undef __STRICT_ANSI__
#include <inttypes.h>
#include <asm/types.h>
#include "ivtv_compat.h"
#include <linux/fb.h>

#ifdef XORG_VERSION_CURRENT
# include "xorgVersion.h"
#endif

#ifdef XSERVER_LIBPCIACCESS
#include <pciaccess.h>
#endif

#define IVTVDEVHWPTR(p) ((ivtvHWPtr)(IVTVDEVHWPTRLVAL(p)))

#define PCI_HAUPPAUGE_VENDOR_ID       0x4444

#if X_BYTE_ORDER==X_BIG_ENDIAN
# define IVTVDEVHW_BYTE_SWAP 1
#endif

#if XSERVER_LIBPCIACCESS
Bool ivtvHWProbe(struct pci_device *PciInfo, char *device, char **namep);
Bool ivtvHWInit(ScrnInfoPtr pScrn, struct pci_device *PciInfo, char *device);
#else
Bool ivtvHWProbe(pciVideoPtr pPci, char *device, char **namep);
Bool ivtvHWInit(ScrnInfoPtr pScrn, pciVideoPtr pPci, char *device);
#endif
char *ivtvHWGetName(ScrnInfoPtr pScrn);
int ivtvHWGetDepth(ScrnInfoPtr pScrn, int *fbbpp);
int ivtvHWGetLineLength(ScrnInfoPtr pScrn);
int ivtvHWGetType(ScrnInfoPtr pScrn);
int ivtvHWGetVidmem(ScrnInfoPtr pScrn);

#ifdef IVTVDEVHW_BYTE_SWAP
void *ivtvHWMapVidmem(ScrnInfoPtr pScrn);
Bool ivtvHWUnmapVidmem(ScrnInfoPtr pScrn);
#endif

void ivtvHWSetVideoModes(ScrnInfoPtr pScrn);
DisplayModePtr ivtvHWGetBuildinMode(ScrnInfoPtr pScrn);
void ivtvHWUseBuildinMode(ScrnInfoPtr pScrn);
Bool ivtvHWModeInit(ScrnInfoPtr pScrn, DisplayModePtr mode);
void ivtvHWSave(ScrnInfoPtr pScrn);
void ivtvHWRestore(ScrnInfoPtr pScrn);
void ivtvHWLoadPalette(ScrnInfoPtr pScrn, int numColors, int *indices,
    LOCO * colors, VisualPtr pVisual);
int ivtvHWValidMode(int scrnIndex, DisplayModePtr mode, Bool verbose, int flags);
Bool ivtvHWSwitchMode(int scrnIndex, DisplayModePtr mode, int flags);
void ivtvHWAdjustFrame(int scrnIndex, int x, int y, int flags);
Bool ivtvHWEnterVT(int scrnIndex, int flags);
void ivtvHWLeaveVT(int scrnIndex, int flags);
void ivtvHWDPMSSet(ScrnInfoPtr pScrn, int mode, int flags);
Bool ivtvHWSaveScreen(ScreenPtr pScreen, int mode);
Bool ivtvHWSendDMA(ScrnInfoPtr pScrn, void *ptr, int x1, int x2, int y1, int y2);

#endif
