/* 
Allows the IVTV X driver to be built to support both the old & new API.

As long as you have valid ivtv.h & videodev2.h files installed from
ivtv 0.7 / linux 2.6.18 or later, this header will allow the driver
to build for both the old & new API.

Copyright (C) 2004-2007 John P. Harvey <john.p.harvey@btinternet.com>
Copyright (C) 2006-2007 Ian Armstrong <ian@iarmst.demon.co.uk>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FIT-
NESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
XFREE86 PROJECT BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Except as contained in this notice, the name of the XFree86 Project shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization from the XFree86 Project.
*/

#ifndef _IVTVCOMPAT_H_
#define _IVTVCOMPAT_H_

#include <sys/time.h>
#include <stdint.h>
#include <linux/types.h>
#include <linux/videodev2.h>
#include <linux/version.h>
#if LINUX_VERSION_CODE < KERNEL_VERSION(2,6,22)
#include <linux/ivtv.h>
#endif

/* Framebuffer external API */
struct ivtvfb_dma_frame
{
    void *source;
    unsigned long dest_offset;
    int count;
};

#ifndef IVTVFB_IOC_DMA_FRAME
# define IVTVFB_IOC_DMA_FRAME  _IOW ('V', BASE_VIDIOC_PRIVATE+0, struct ivtvfb_dma_frame)
#endif

#ifndef IVTV_IOC_DMA_FRAME
# define IVTV_IOC_DMA_FRAME _IOW ('V', BASE_VIDIOC_PRIVATE+0, struct ivtv_dma_frame)
struct ivtv_dma_frame
{
    enum v4l2_buf_type type;
    __u32 pixelformat;
    void *y_source;
    void *uv_source;
    struct v4l2_rect src;
    struct v4l2_rect dst;
    __u32 src_width;
    __u32 src_height;
};
#endif

#ifndef IVTV_IOC_PREP_FRAME_YUV
# define IVTV_IOC_PREP_FRAME_YUV  _IOW('@', 60, struct ivtvyuv_ioctl_dma_host_to_ivtv_args)
struct ivtvyuv_ioctl_dma_host_to_ivtv_args
{
    void *y_source;
    void *uv_source;
    unsigned int yuv_type;
    int src_x;
    int src_y;
    unsigned int src_w;
    unsigned int src_h;
    int dst_x;
    int dst_y;
    unsigned int dst_w;
    unsigned int dst_h;
    int srcBuf_width;
    int srcBuf_height;
};
#endif

#ifndef IVTV_IOC_G_YUV_INTERLACE
# define IVTV_IOC_G_YUV_INTERLACE   _IOR ('@', 61, struct ivtv_ioctl_yuv_interlace)
# define IVTV_IOC_S_YUV_INTERLACE   _IOW ('@', 62, struct ivtv_ioctl_yuv_interlace)
# define IVTV_YUV_MODE_INTERLACED	0x00
# define IVTV_YUV_MODE_PROGRESSIVE	0x01
# define IVTV_YUV_MODE_AUTO		0x02
struct ivtv_ioctl_yuv_interlace
{
    int interlace_mode;
    int threshold;
};
#endif //IVTV_IOC_G_YUV_INTERLACE

#ifndef IVTV_YUV_MODE_MASK
# define IVTV_YUV_MODE_MASK	0x03
# define IVTV_YUV_SYNC_EVEN	0x00
# define IVTV_YUV_SYNC_ODD	0x04
#endif //IVTV_YUV_MODE_MASK

#ifndef V4L2_CAP_VIDEO_OUTPUT_OVERLAY
# define V4L2_BUF_TYPE_VIDEO_OUTPUT_OVERLAY 8
# define V4L2_FIELD_INTERLACED_TB 8
# define V4L2_FIELD_INTERLACED_BT 9
#endif

#ifndef V4L2_FBUF_FLAG_LOCAL_ALPHA
# define V4L2_FBUF_FLAG_LOCAL_ALPHA	0x0008
# define V4L2_FBUF_FLAG_GLOBAL_ALPHA	0x0010
#endif

#ifndef IVTV_IOC_GET_FB
# define IVTV_IOC_GET_FB            _IOR('@', 44, int)
# define IVTVFB_IOCTL_GET_STATE     _IOR('@', 1, struct ivtvfb_ioctl_state_info)
# define IVTVFB_IOCTL_SET_STATE     _IOW('@', 2, struct ivtvfb_ioctl_state_info)
# define IVTVFB_IOCTL_PREP_FRAME    _IOW('@', 3, struct ivtvfb_dma_frame)
# define IVTVFB_STATUS_ENABLED      (1 << 0)
# define IVTVFB_STATUS_GLOBAL_ALPHA (1 << 1)
# define IVTVFB_STATUS_LOCAL_ALPHA  (1 << 2)
struct ivtvfb_ioctl_state_info {
    unsigned long status;
    unsigned long alpha;
};
#endif

#ifndef IVTVFB_IOCTL_SET_COLORKEY
# define IVTVFB_IOCTL_SET_COLORKEY  _IOW('@', 13, struct ivtvfb_ioctl_colorkey)
struct ivtvfb_ioctl_colorkey
{
    int state;
    __u32 colorKey;
};
#endif

#ifndef V4L2_PIX_FMT_HM12
# define V4L2_PIX_FMT_HM12 v4l2_fourcc('H','M','1','2')
#endif

#endif
