/*
X11 video driver for the Conexant 23415 MPEG encoder/decoder On-Screen Display
using the ivtvfb framebuffer driver.

Copyright (C) 2004 Matthias Badaire
Copyright (C) 2004-2007 John P. Harvey <john.p.harvey@btinternet.com>
Copyright (C) 2006-2007 Ian Armstrong <ian@iarmst.demon.co.uk>

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the "Software"), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FIT-
NESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.  IN NO EVENT SHALL THE
XFREE86 PROJECT BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN
AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

Except as contained in this notice, the name of the XFree86 Project shall not
be used in advertising or otherwise to promote the sale, use or other dealings
in this Software without prior written authorization from the XFree86 Project.
*/

#ifndef _IVTV_H_
#define _IVTV_H_

#define IVTVDEBUG 0

typedef struct
{
    /* framebuffer device: filename (/dev/fb*), handle, more */
    char *device;
    int fd;
    char *yuvDevName;
    int yuvDevNum;
    int fd_yuv;
    void *fbmem;
    unsigned int fbmem_len;
    unsigned int fboff;

    Bool legacy_dma;
    Bool legacy_api;
    Bool legacy_fb_api;
    Bool dma_write;

    /* current hardware state */
    struct fb_fix_screeninfo fix;
    struct fb_var_screeninfo var;

    /* saved video mode */
    struct fb_var_screeninfo saved_var;

    /* buildin video mode */
    DisplayModeRec buildin;
    int lineLength;
    CloseScreenProcPtr CloseScreen;
    EntityInfoPtr pEnt;
    OptionInfoPtr Options;
    CreateScreenResourcesProcPtr CreateScreenResources;

    unsigned char *shadowmem;
    int shadow_width;
    int shadow_height;

    unsigned int xv_colorKey;
    RegionRec xv_clip;
    int xv_autopaintColorKey;
    unsigned char *xv_buffer;

    Bool xv_interlaceControl;
    int xv_fieldTopFirst;
    int xv_fieldOverride;
    int xv_interlaced;

    struct v4l2_format alphaState;
    struct v4l2_framebuffer fbState;
    struct v4l2_format pixFmt;
    struct v4l2_crop pixCrop;
} IVTVDevRec, *IVTVDevPtr;

#define IVTVDEVPTR(p) ((IVTVDevPtr)((p)->driverPrivate))

#endif
